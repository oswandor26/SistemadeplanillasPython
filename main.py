from re import RegexFlag
from time import sleep
from datetime import datetime
from tkinter.font import BOLD
from rich.prompt import Console, Prompt, Confirm
from rich.progress import track
from rich.table import Table
from rich.align import Align
from rich.layout import Layout
from rich.live import Live
from rich.text import Text
from rich.layout import Layout
from rich.panel import Panel
from rich import box
from rich.console import Group
import pandas as pd

console = Console()
layout = Layout()

ISS = 0.03
AFP = 0.0725

# funcion principla para obtener los empleados


def obtenerdatosempleados():

    # se crean las varibles tipo globales para poder acceder en otras funciones
    global lista_de_empleados
    global cantidadEmpleadosAgregar

    lista_de_empleados = {
        "codigo": [],
        "nombre": [],
        "Sueldoempelado": [],
        "ISS": [],
        "AFP": [],
        "SAR": [],
        "RENTA": [],
        "sueldomesualfinal": []
    }

    cantidadEmpleadosAgregar = Prompt.ask(
        ":atom_symbol: Ingrese el numero de empleados [b]1[/b] a [b]n...[/b]", default=1)

    # barra de carga
    for n in track(range(int(cantidadEmpleadosAgregar)), "Procesando..."):
        sleep(0.4)

    # añadir datos de empleados
    contadordefault = 1
    for i in range(int(cantidadEmpleadosAgregar)):

        codigo = Prompt.ask(
            ":green_book: Ingrese su codigo de Empleado", default=str(contadordefault))
        sueldo = Prompt.ask("Ingrese su sueldo Mensual", default="300")
        nombre = Prompt.ask("Ingrese su Nombre completo",
                            default="Paul Atreides")

        # convertir a float
        sueldo = float(sueldo)

        # se calculan los descuentos ISSS y AFP
        descuentoISSS = sueldo * ISS
        descuentoAFP = sueldo * AFP

        sueldoantedelarenta = sueldo - (descuentoISSS + descuentoAFP)

        # se calcula la renta
        if sueldo > 1 and sueldo <= 500:
            renta = 0
        elif sueldo > 500 and sueldo <= 1000:
            renta = 0.10
        elif sueldo > 1000:
            renta = 0.20

        descuendorenta = sueldoantedelarenta * renta

        lista_de_empleados["codigo"].append(codigo)
        lista_de_empleados["nombre"].append(nombre)
        lista_de_empleados["Sueldoempelado"].append(format(sueldo, '.2f'))
        lista_de_empleados["ISS"].append(format(descuentoISSS, '.2f'))
        lista_de_empleados["AFP"].append(format(descuentoAFP, '.2f'))
        lista_de_empleados["SAR"].append(format(sueldoantedelarenta, '.2f'))
        lista_de_empleados["RENTA"].append(format(descuendorenta, '.2f'))
        lista_de_empleados["sueldomesualfinal"].append(
            format((sueldoantedelarenta - descuendorenta), '.2f'))

        contadordefault += 1
        print(lista_de_empleados)


# para volver a calcular  el sueldo
def hacercalculo(sueldo: float, idmodificar):

    # se calculan los descuentos ISSS y AFP
    descuentoISSS = sueldo * ISS
    descuentoAFP = sueldo * AFP

    sueldoantedelarenta = sueldo - (descuentoISSS + descuentoAFP)

    # se calcula la renta
    if sueldo > 1 and sueldo <= 500:
        renta = 0
    elif sueldo > 500 and sueldo <= 1000:
        renta = 0.10
    elif sueldo > 1000:
        renta = 0.20

    descuendorenta = sueldoantedelarenta * renta

    lista_de_empleados["Sueldoempelado"][idmodificar] = format(sueldo, '.2f')
    lista_de_empleados["ISS"][idmodificar] = format(descuentoISSS, '.2f')
    lista_de_empleados["AFP"][idmodificar] = format(descuentoAFP, '.2f')
    lista_de_empleados["SAR"][idmodificar] = format(sueldoantedelarenta, '.2f')
    lista_de_empleados["RENTA"][idmodificar] = format(descuendorenta, '.2f')
    lista_de_empleados["sueldomesualfinal"][idmodificar] = format(
        (sueldoantedelarenta - descuendorenta), '.2f')


# para modificar los empleados
def modificardatosdeempleado(idnumber: int):

    # el numero  de indice del array
    numeroid = lista_de_empleados["codigo"].index(str(idnumber))

    quequieremodificar = Prompt.ask("Que desea modificar?", choices=[
                                    "nombre", "sueldo"], default="nombre")

    if quequieremodificar == "nombre":

        nombrenuevo = Prompt.ask("Ingrese el nuevo nombre: ")
        lista_de_empleados["nombre"][numeroid] = str(nombrenuevo)
        print(lista_de_empleados)

    elif quequieremodificar == "sueldo":
        nuevosueldo = Prompt.ask("Ingrese el nuevo sueldo: ")
        hacercalculo(float(nuevosueldo), numeroid)
        print(lista_de_empleados)


def elimiarempleados(idnumber: int):
    pass


# para mostrar la tabla
def mostrarempleados():

    print("\n")
    table = Table(title="Planilla de Empleados")

    table.add_column("Codigo", justify="right", style="cyan", no_wrap=True)
    table.add_column("Nombre", style="magenta")
    table.add_column("Sueldo", justify="right", style="green")
    table.add_column("ISSS", justify="right", style="cyan", no_wrap=True)
    table.add_column("AFP", style="magenta")
    table.add_column("SAR", justify="right", style="green")
    table.add_column("RENTA", justify="right", style="cyan", no_wrap=True)
    table.add_column("Sueldo Final", style="magenta")

    # obtube los datos del diccionario en un  array de 2 dimensiones
    listadeparatablas = []
    for key, valor in lista_de_empleados.items():
        listadeparatablas.append(valor)

    # se ordena el array por filas
    daframe = pd.DataFrame(listadeparatablas, columns=[str(
        x) for x in range(0, int(cantidadEmpleadosAgregar))])

    nuevalista = []
    for column in daframe:
        nuevalista.append(daframe[column].values)

    for i in range(len(nuevalista)):
        table.add_row(
            str(nuevalista[i][0]),
            str(nuevalista[i][1]),
            str(nuevalista[i][2]),
            str(nuevalista[i][3]),
            str(nuevalista[i][4]),
            str(nuevalista[i][5]),
            str(nuevalista[i][6]),
            str(nuevalista[i][7]))

    console = Console()
    console.print(table)

# Inicializar panel


def inicializarPanel() -> Layout:
    """Define the layout."""
    layout = Layout(name="root")
    layout.split(
        Layout(name="header", size=3),
        Layout(name="main")
    )

    layout["main"].split_row(Layout(name="body", ratio=2))

    def body() -> Panel:
        body = Table.grid()
        body.add_column(style="green", justify="center")
        body.add_row(
            "1.", " Obtener Empleados"
        )
        body.add_row(
            "2.", " Realizar calculo"
        )
        body.add_row(
            "3.", " Modificar datos"
        )
        body.add_row(
            "4.", " Eliminar registro"
        )
        body.add_row(
            "5.", " Salir"
        )
        intro_message = Text.from_markup(
            """Menú"""
        )

        message = Table.grid(padding=1)
        message.add_column()
        message.add_row(intro_message, body)

        message_panel = Panel(
            Align.center(
                Group(intro_message, "\n", Align.center(body)),
                vertical="middle",
            ),
            box=box.ROUNDED,
            border_style="bright_blue",
        )
        return message_panel

    layout["body"].update(body())

    class Header:

        def __rich__(self) -> Panel:
            grid = Table.grid(expand=True)
            grid.add_column(justify="center", ratio=1)
            grid.add_row("[b]Los necesitados[/b]",
                         datetime.now().ctime().replace(":", "[blink]:[/]"))
            return Panel(grid)

    layout["header"].update(Header())

    return layout


# Panel principal
def panelPricipal():
    layout = inicializarPanel()
    console.print(layout)
    try:
        opc = Prompt.ask(
            ":atom_symbol: Seleccione una opción [b]1[/b] a [b]5[/b]", default=5)
        while int(opc) != 0:
            if int(opc) == 1:
                for n in track(range(int(opc)), "Procesando..."):
                    sleep(0.4)
                obtenerdatosempleados()
            elif int(opc) == 2:
                for n in track(range(int(opc)), "Procesando..."):
                    sleep(0.4)
                id = int(input("ingresar el id:"))
                modificardatosdeempleado(id)
            elif int(opc) == 3:
                for n in track(range(int(opc)), "Procesando..."):
                    sleep(0.4)
                id = int(input("ingresar el id:"))
                elimiarempleados(id)
            elif int(opc) == 4:
                for n in track(range(int(opc)), "Procesando..."):
                    sleep(0.4)
                mostrarempleados()
            elif int(opc) == 5:
                # barra de carga
                for n in track(range(int(opc)), "Finalizando..."):
                    sleep(0.8)
                break
            else:
                print("Introduce un numero entre 1 y 5")
            layout = inicializarPanel()
            console.print(layout)
            opc = Prompt.ask(
                ":atom_symbol: Seleccione una opción [b]1[/b] a [b]5[/b]", default=5)
    except ValueError as e:
        print("Por favor introduce un valor numerico")
        return   


if __name__ == "__main__":
    try:
        panelPricipal()
    except Exception as e:
        print(e)
    # try:
        # el orden seria obtenerdatosempleados()    ->   mostrarempleados()
        # obtenerdatosempleados()

        #deseamodificar = Confirm.ask("desamodificar?")
        # if deseamodificar =="yes" or deseamodificar == "y":
        #id = int(input("ingresar el id:"))
        # modificardatosdeempleado(id)

        #is_rich_great = Confirm.ask("Do you like rich?")

    # except Exception as e:
        # print(e)
